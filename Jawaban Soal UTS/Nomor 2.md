**DDL (Data Definition Language)**

```sql
-- Table users
CREATE TABLE `Users` (
  -- Menjadikan kolom id sebagai induk/id utama table Users
  -- kolom id akan terus bertambah secara otomatis melalui auto increment
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  -- kolom username memiliki constraint unique
  -- hal ini memungkinkan username antar 1 user dengan yang lainnya tidak sama
  `username` varchar(255) UNIQUE,
  `nickname` varchar(255),
  `first_name` varchar(255),
  `last_name` varchar(255),
  `email` varchar(255) UNIQUE,
  `password` varchar(255),
  `profil_pic` varchar(255),
  `bio` varchar(255),
  `location` varchar(255),
  `birthdate` DATE,
  `join_date` DATE,
  `gender` boolean,
  `acc_type` boolean
);

-- Table tweets
CREATE TABLE `Tweets` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `user_id` int,
  `tweet_text` TEXT,
  `tweet_score` integer,
  `created_at` DATETIME,
  `retweet_count` integer,
  `like_count` integer
);

-- Table likes
CREATE TABLE `Likes` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `user_id` int,
  `tweet_id` int,
  `created_at` DATETIME
);

-- Table comments
CREATE TABLE `Comments` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `user_id` int,
  `tweet_id` int,
  `comment_text` TEXT,
  `created_at` DATETIME
);

-- Table retweets
CREATE TABLE `Retweets` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `retweet_text` varchar(255),
  `user_id` int,
  `tweet_id` int,
  `created_at` DATETIME
);

-- Table mentions
CREATE TABLE `Mentions` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `tweet_id` int,
  `user_id` int
);

-- Table replies
CREATE TABLE `Replies` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `user_id` int,
  `tweet_id` int,
  `reply_text` TEXT,
  `created_at` DATETIME,
  `reply_to_tweet_id` int
);

-- Table bookmarks
CREATE TABLE `Bookmarks` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `user_id` int,
  `tweet_id` int
);

-- Table messages
CREATE TABLE `DM` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `message` TEXT,
  `sender_id` int,
  `receiver_id` int,
  `created_at` DATETIME
);

-- Table followers
CREATE TABLE `Followers` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `follower_id` int,
  `following_id` int
);

-- Table following
CREATE TABLE `Following` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `user_id` int,
  `followed_user_id` int
);

-- Pada table Tweets menambahkan user_id sebagai foreign key dari table Users kolom id
ALTER TABLE `Tweets` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Likes` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Likes` ADD FOREIGN KEY (`tweet_id`) REFERENCES `Tweets` (`id`);

ALTER TABLE `Comments` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Comments` ADD FOREIGN KEY (`tweet_id`) REFERENCES `Tweets` (`id`);

ALTER TABLE `Retweets` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Retweets` ADD FOREIGN KEY (`tweet_id`) REFERENCES `Tweets` (`id`);

ALTER TABLE `Mentions` ADD FOREIGN KEY (`tweet_id`) REFERENCES `Tweets` (`id`);

ALTER TABLE `Mentions` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Replies` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Replies` ADD FOREIGN KEY (`tweet_id`) REFERENCES `Tweets` (`id`);

ALTER TABLE `Replies` ADD FOREIGN KEY (`reply_to_tweet_id`) REFERENCES `Tweets` (`id`);

ALTER TABLE `Bookmarks` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Bookmarks` ADD FOREIGN KEY (`tweet_id`) REFERENCES `Tweets` (`id`);

ALTER TABLE `DM` ADD FOREIGN KEY (`sender_id`) REFERENCES `Users` (`id`);

ALTER TABLE `DM` ADD FOREIGN KEY (`receiver_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Followers` ADD FOREIGN KEY (`follower_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Followers` ADD FOREIGN KEY (`following_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Following` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Following` ADD FOREIGN KEY (`followed_user_id`) REFERENCES `Users` (`id`);

```
