**USE CASE OPERASIONAL CRUD**

| Use Case | Priority |
| ------ | ------ |
| User Registration       | HIGH |
| User Login       | HIGH |
| Update User     | HIGH |
| Follow another User     | HIGH |
| Delete User     | HIGH |
| Create Tweet       | HIGH |
| Display Tweet       | HIGH |
| Edit Tweet       | HIGH |
| Delete Tweet       | HIGH |

**DATA MANIPULATION LANGUAGE (DML)**
User Registration
```sql
INSERT INTO Users (username, nickname, first_name, last_name, gender, email, password, birthdate, join_date, acc_type)
VALUES 
('andi123', 'Andi', 'Andi', 'Susanto', 1, 'andi.susanto@gmail.com', 'password123', '1990-01-01', '2022-01-01', 0),
('budi456', 'Budi', 'Budi', 'Hermawan', 0, 'budi.hermawan@gmail.com', 'password456', '1995-03-15', '2022-01-01', 1),
('citra789', 'Citra', 'Citra', 'Kusuma', 1, 'citra.kusuma@gmail.com', 'password789', '1992-08-12', '2022-01-02', 0),
('dewi012', 'Dewi', 'Dewi', 'Santoso', 1, 'dewi.santoso@gmail.com', 'password012', '1988-12-31', '2022-01-02', 1),
('eko345', 'Eko', 'Eko', 'Purnomo', 0, 'eko.purnomo@gmail.com', 'password345', '1997-05-22', '2022-01-03', 0),
('fina678', 'Fina', 'Fina', 'Wijaya', 1, 'fina.wijaya@gmail.com', 'password678', '1999-09-08', '2022-01-03', 1),
('gita901', 'Gita', 'Gita', 'Sari', 1, 'gita.sari@gmail.com', 'password901', '1994-06-10', '2022-01-04', 0),
('hadi234', 'Hadi', 'Hadi', 'Saputra', 0, 'hadi.saputra@gmail.com', 'password234', '1996-11-17', '2022-01-04', 1),
('irma567', 'Irma', 'Irma', 'Fauziyah', 1, 'irma.fauziyah@gmail.com', 'password567', '1991-04-30', '2022-01-05', 0),
('jaka890', 'Jaka', 'Jaka', 'Pratama', 0, 'jaka.pratama@gmail.com', 'password890', '1993-07-18', '2022-01-05', 1);
```
User Login
```sql
SELECT * FROM Users WHERE username = 'andi123' and PASSWORD = 'password123';
```
Update User
```sql
UPDATE Users
SET bio = CASE
    WHEN id = 1 THEN 'Halo, saya Andi. Saya senang bermain basket dan mendengarkan musik.'
    WHEN id = 2 THEN 'Halo, saya Budi. Saya suka memasak dan bermain game.'
    WHEN id = 3 THEN 'Halo, saya Citra. Saya senang traveling dan mendengarkan podcast.'
    WHEN id = 4 THEN 'Halo, saya Dewi. Saya suka membaca dan bersepeda.'
    WHEN id = 5 THEN 'Halo, saya Eko. Saya senang fotografi dan hiking.'
    WHEN id = 6 THEN 'Halo, saya Fina. Saya suka menulis dan bermain gitar.'
    WHEN id = 7 THEN 'Halo, saya Gita. Saya senang menggambar dan berolahraga.'
    WHEN id = 8 THEN 'Halo, saya Hadi. Saya suka menonton film dan bermain bulu tangkis.'
    WHEN id = 9 THEN 'Halo, saya Irma. Saya senang memasak dan berkebun.'
    WHEN id = 10 THEN 'Halo, saya Jaka. Saya suka mendengarkan musik dan bermain futsal.'
    ELSE bio
END,
location = CASE
    WHEN id = 1 THEN 'Jakarta'
    WHEN id = 2 THEN 'Bandung'
    WHEN id = 3 THEN 'Surabaya'
    WHEN id = 4 THEN 'Surabaya'
    WHEN id = 5 THEN 'Bali'
    WHEN id = 6 THEN 'Surabaya'
    WHEN id = 7 THEN 'Bandung'
    WHEN id = 8 THEN 'Bandung'
    WHEN id = 9 THEN 'Jakarta'
    WHEN id = 10 THEN 'Bali'
    ELSE location
END
WHERE id BETWEEN 1 AND 10;
```
Follow another User
```sql
INSERT INTO Followers (follower_id, following_id)
VALUES 
	(21, 1), (22, 1), (23, 1), (24, 1), (25, 1), (26, 1), (27, 1), (28, 1), (29, 1), (30, 1);
```
Delete User
```sql
DELETE FROM Users
WHERE username = 'andi123';
```
Create Tweet
```sql
INSERT INTO Tweets (user_id, tweet_text, created_at) VALUES
(1, 'Ini tweet dummy pertama', NOW()),
(2, 'Ini tweet dummy kedua', NOW()),
(3, 'Ini tweet dummy ketiga', NOW()),
(1, 'Ini tweet dummy keempat', NOW()),
(2, 'Ini tweet dummy kelima', NOW());
```
Display Tweet
```sql
SELECT * FROM Tweets;
```
Edit Tweet
```sql
UPDATE Tweets
SET tweet_text = "Hello World"
WHERE id = 1;
```
Delete Tweet
```sql
DELETE FROM Tweets
WHERE id = 5;
```
