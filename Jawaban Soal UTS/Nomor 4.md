**DATA QUERY LANGUAGE (DQL)**
Pertanyaan 1 : Berapa jumlah users tweetify berdasarkan gender?
```sql
SELECT
    CASE
        WHEN gender = 0 THEN 'Laki-laki'
        WHEN gender = 1 THEN 'Perempuan'
    END AS gender,
    COUNT(*) AS total_users
FROM Users
GROUP BY gender;

```
Pertanyaan 2 : Hitung perbandingan jumlah user regular dan premium!
```sql
SELECT
	CASE
		WHEN acc_type = 0 THEN 'Regular'
		WHEN acc_type = 1 THEN 'Premium'
	END AS Tipe_Akun,
	COUNT(*) AS Jumlah_User
FROM Users
GROUP BY Tipe_Akun;
```
Pertanyaan 3 : Bagaimana trend pertumbuhan jumlah pengguna Tweetify dari waktu ke waktu?
```sql
SELECT YEAR(join_date) AS tahun, MONTH(join_date) AS bulan, COUNT(*) AS jumlah_pengguna
FROM Users
GROUP BY YEAR(join_date), MONTH(join_date)
ORDER BY tahun, bulan;
```
Pertanyaan 4 : Hitung Tweet dengan jumlah like terbanyak!
```sql
SELECT tweet_text, like_count, Users.username
FROM Tweets
JOIN Users ON Tweets.user_id = Users.id
WHERE like_count = (SELECT MAX(like_count) FROM Tweets);
```
Pertanyaan 5 : Siapa saja influencer atau pengguna dengan followers terbesar di tweetify?
```sql
SELECT username, COUNT(*) as followers_count
FROM Followers
JOIN Users ON Followers.following_id = Users.id
GROUP BY following_id
ORDER BY followers_count DESC
LIMIT 3;
```
Pertanyaan 6 : Bagaimana distribusi umur pengguna tweetify?
```sql
SELECT 
    CASE 
        WHEN TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 0 AND 17 THEN '0-17' 
        WHEN TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 18 AND 24 THEN '18-24' 
        WHEN TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 25 AND 34 THEN '25-34' 
        WHEN TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 35 AND 44 THEN '35-44' 
        WHEN TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 45 AND 54 THEN '45-54' 
        WHEN TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 55 AND 64 THEN '55-64' 
        WHEN TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) >= 65 THEN '65+' 
    END AS age_range, 
    COUNT(*) AS user_count 
FROM Users 
GROUP BY age_range;
```

